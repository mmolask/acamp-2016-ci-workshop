from flask import Flask
from flask import render_template, request
from greetlib.greet import Greet
from os import environ
import hashlib

app = Flask(__name__)
app.debug = True

@app.route("/")
def index():
    email = request.args.get('email', 'npaolucci@atlassian.com')
    email_md5 = hashlib.md5(email.encode('utf8')).hexdigest()
    gravatar = "http://www.gravatar.com/avatar/%s?s=512" % email_md5
    locale = environ.get('GREET_LOC', 'en')
    message = "{} World!".format(Greet.forLocale(locale))

    return render_template('index.html', message=message, gravatar=gravatar)

if __name__ == "__main__":
    app.run(host='0.0.0.0')
